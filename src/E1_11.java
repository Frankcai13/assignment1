// class E1_11
// Author: Frank Cai
// Date created: 2/20/2021
// Program to display Yoda from Star Wars

public class E1_11 {
    public static void main(String[] args){
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        printOneLine("__.-.__      __________________");
        printOneLine("'-o_o-'     / Hi, Yoda I am.   \\");
        printOneLine(" /'.-c     <  Do or do not,    |");
        printOneLine(" |  /T      \\ there is no try  |");
        printOneLine("_)_/LI       \\_________________/");
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
    }

    // function to print out one line
    public static void printOneLine( String strToPrint ){
        System.out.print("                 ");
        System.out.println(strToPrint);
    }
}
